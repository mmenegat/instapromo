/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src';
import {name as appName} from './app.json';
import Amplify from 'aws-amplify';
import { MemoryStorageNew } from './src/pages/auth/MemoryStorageNew';
import config from './config';

const apiGateway = config.API_GATEWAY;
const region = config.AWS_REGION;

Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: config.AWS_REGION,
    userPoolId: config.AWS_USER_POOL,
    identityPoolId: config.AWS_IDENTITY_POOL,
    userPoolWebClientId : config.AWS_USER_POOL_WEB_CLIENT_ID,
    storage: MemoryStorageNew
  },
  API: {
    endpoints: [
      {
        name : "getPromotionsByRegion",
        endpoint : apiGateway + "getPromotionsByRegion",
        region: region
      },
      {
        name : "userprofile",
        endpoint :  apiGateway + "userprofile",
        region: region
      },
      {
        name : "getuserprofile",
        endpoint :  apiGateway + "getuserprofile",
        region: region
      },
      {
        name : "createVoucher",
        endpoint :  apiGateway + "createVoucher",
        region: region
      },
      {
        name : "getVouchers",
        endpoint :  apiGateway + "getVouchers",
        region: region
      },
      {
        name : "verifyCreateVoucher",
        endpoint : apiGateway + "verifyCreateVoucher",
        region: region
      },
      {
        name : "reportProblem",
        endpoint :  apiGateway + "reportProblem",
        region: region
      }
      
    ],
  },
});

AppRegistry.registerComponent(appName, () => App);
