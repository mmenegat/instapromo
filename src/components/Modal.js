import Modal from 'react-native-modalbox';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'native-base';
import { Colours } from '../styles/Styles';

export const ShowMessage = props => {
  return (
    <Modal
      style={styles.mainContainer}
      coverScreen={true}
      backdropPressToClose={false}
      swipeToClose={false}
      isOpen={props.isOpen}
      position={'center'}>
      <Text style={styles.title}>{props.title}</Text>
      <Text style={styles.description}>{props.description}</Text>
      <View style={styles.lineStyle} />
      <Text style={[styles.button, styles.ok]} onPress={props.onClose}>
        OK
      </Text>
    </Modal>
  );
};

export const ConfirmMessage = props => {
  return (
    <Modal
      style={styles.mainContainer}
      isOpen={props.isOpen}
      coverScreen={true}
      backdropPressToClose={false}
      swipeToClose={false}
      position={'center'}>
      <Text style={styles.title}>{props.title}</Text>
      <Text style={styles.description}>{props.description}</Text>
      <View style={styles.lineStyle} />
      <View style={styles.buttonBar}>
        <Text style={[styles.button, styles.cancel]} onPress={props.onCancel}>
          Cancelar
        </Text>
        <Text style={[styles.button, styles.confirm]} onPress={props.onConfirm}>
          Confirmar
        </Text>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: Colours.WHITE,
    height: 180,
    width: 280,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: Colours.DARK_BLUE,
    fontSize: 20,
    fontWeight: 'bold',
  },
  description: {
    color: Colours.DARK_BLUE,
    marginTop: 20,
    marginBottom: 20,
  },
  button: {
    color: Colours.ORANGE,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  ok: {
    width: '100%',
  },
  confirm: {
    width: '50%',
  },
  cancel: {
    width: '50%',
  },
  lineStyle: {
    borderWidth: 0.5,
    width: '100%',
    borderColor: Colours.DARK_BLUE,
    margin: 10,
  },
  buttonBar: {
    flexDirection: 'row',
    width: 280,
  },
});
