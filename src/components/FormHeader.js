import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Header, Right, Body, Icon, Button } from 'native-base';
import { Colours } from '../styles/Styles';
import Divider from './Divider';

const FormHeader = props => {
  let { title, onPressBack, onLogout, onRefresh } = props;

  return (
    <>
      <Header style={styles.header} androidStatusBarColor={Colours.WHITE}>
        <Body>
          <View style={styles.container}>
            {onPressBack && (
              <Button transparent onPress={onPressBack}>
                <Icon
                  style={styles.iconLeft}
                  type="MaterialIcons"
                  name="keyboard-backspace"
                />
              </Button>
            )}
            <Text style={styles.headerText}>{title}</Text>

            {onLogout && (
              <Right>
                <Button transparent onPress={onLogout}>
                  <Icon
                    style={{ color: Colours.DARK_BLUE }}
                    type="MaterialIcons"
                    name="exit-to-app"
                  />
                </Button>
              </Right>
            )}

            {onRefresh && (
              <Right>
                <Button transparent onPress={onRefresh}>
                  <Icon
                    style={{ color: Colours.DARK_BLUE }}
                    type="MaterialIcons"
                    name="update"
                  />
                </Button>
              </Right>
            )}
          </View>
          <Divider />
        </Body>
      </Header>
    </>
  );
};

export default FormHeader;

const styles = StyleSheet.create({
  header: {
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0,
    borderBottomWidth: 0,
    backgroundColor: Colours.WHITE,
  },
  headerText: {
    fontSize: 36,
    color: Colours.ORANGE,
    fontWeight: 'bold',
  },
  container: {
    width: '100%',
    flexDirection: 'row',
    marginLeft: 15,
  },
  iconRight: {
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
  },
  iconLeft: {
    color: Colours.DARK_BLUE,
    marginLeft: -4,
  },
});
