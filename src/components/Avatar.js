import React, { useState } from 'react';
import { StyleSheet, Image } from 'react-native';
import { getProfilePhoto } from '../pages/auth/auth';
import { isIphoneX } from '../util/util';
import ProgressiveImage from './ProgressiveImage';
import userLogo from '../assets/user.png';

const iphoneX = isIphoneX();

const Avatar = props => {
  const [img, setImg] = useState('');
  const [hasLoaded, setHasLoaded] = useState(false);

  if (!hasLoaded) {
    getProfilePhoto()
      .then(res => {
        if(res){
          setImg(res);
        }
        setHasLoaded(true);
      })
      .catch(err => {});
  }

  if(img === ''){
    return (
      <Image
        style={iphoneX ? styles.loginX : styles.login}
        source={userLogo}
      />
    );
  }

  return (
    <Image
      style={iphoneX ? styles.loginX : styles.login}
      source={{ uri: img }}
    />
  );
};

export default Avatar;

const base = {
  width: iphoneX ? 140 : 120,
  height: iphoneX ? 140 : 120,
  borderRadius: 10,
  borderWidth: 4,
  borderColor: 'white',
  marginBottom: 10,
  position: 'absolute',
};

const styles = StyleSheet.create({
  login: {
    ...base,
    alignSelf: 'center',
    marginTop: 70,
  },
  loginX: {
    ...base,
    alignSelf: 'center',
    marginTop: 120,
  },
});
