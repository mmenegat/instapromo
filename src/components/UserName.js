import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { getUsername } from '../pages/auth/auth';
import { Colours } from '../styles/Styles';

const UserName = props => {
  const [hasLoaded, setHasLoaded] = useState(false);
  const [uName, setUName] = useState('');
  let { type, img } = props;

  if (!hasLoaded) {
    getUsername()
      .then(res => {
        setUName(res);
        setHasLoaded(true);
      })
      .catch(err => alert(err));
  }

  if (img === undefined) {
    img = 'https://bootdey.com/img/Content/avatar/avatar6.png';
  }

  if (type === 'ios') {
    return (
      <View style={{ flex: 1, marginBottom: 55 }}>
        <Text style={styles.ios}>{uName}</Text>
      </View>
    );
  }

  if (type === 'android') {
    return <Text style={styles.androidCenter}>{uName}</Text>;
  }

  if (type === 'large') {
    return <Text style={styles.large}>{uName}</Text>;
  }

  return <Text style={styles.androidRight}>{uName}</Text>;
};

export default UserName;

const base = {
  fontSize: 22,
  color: Colours.ORANGE,
  fontWeight: '600',
};

const styles = StyleSheet.create({
  ios: {
    ...base,
    marginBottom: 10,
    marginTop: 65,
    alignSelf: 'center',
    position: 'absolute',
  },

  androidCenter: {
    ...base,
    marginTop: 47,
    marginBottom: 10,
    alignSelf: 'center',
  },

  large: {
    ...base,
    marginTop: 5,
    marginBottom: 10,
    alignSelf: 'center',
  },

  androidRight: {
    ...base,
    alignSelf: 'flex-end',
    marginTop: 130,
    position: 'absolute',
    paddingRight: 15,
  },
});
