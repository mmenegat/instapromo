import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ActivityIndicator,
  StyleSheet,
  View,
  Dimensions,
  Text
} from 'react-native';

let color_spinner = 'black'
let color_background = "#FFFFFF"

let { width, height } = Dimensions.get('window');

const Loader = props => {
  const { isLoading } = props;

  if (!isLoading) {
    return null;
  }

  if (props.mode && props.mode === "modal" ){
    color_spinner = '#0275d8'
    color_background = "#FFECEC"
  }

  let styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      width: width,
      height: height,
      position: 'absolute',
      top: 0,
      left: 0,
      backgroundColor: color_background,
      opacity: 0.5,
      zIndex: 100,
    },
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      padding: 10,
    },
  });


  return (
    <View {...this.props} style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="large" color={color_spinner}/>
    </View>
  );
};

Loader.propTypes = {
  isLoading: PropTypes.oneOfType([PropTypes.bool]).isRequired,
};

export default Loader;
