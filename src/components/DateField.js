import React, { useState } from 'react';
import { Alert, StyleSheet } from 'react-native';
import { Form, Item, Picker, Input, Label } from 'native-base';
import { Colours } from '../styles/Styles';

const DateField = props => {
  let { bornDate, label } = props;

  const [dd, setDD] = useState(bornDate ? bornDate.substring(0, 2) : '');
  const [mm, setMM] = useState(bornDate ? bornDate.substring(3, 5) : '');
  const [yyyy, setYYYY] = useState(bornDate ? bornDate.substring(6, 10) : '');

  const isValidDate = (day, month, year) => {
    if (year < 1000 || year > 3000 || month == 0 || month > 12) return false;

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
      monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
  };

  const checkDate = (day, month, year) => {
    let valid = false;

    if (day > 0 && month > 0 && year > 1900) {
      if (isValidDate(day, month, year)) {
        props.onChange(
          ('0' + day).slice(-2) + '-' + ('0' + month).slice(-2) + '-' + year,
        );
        valid = true;
      }

      if (!valid) {
        Alert.alert(
          'Alerta',
          'Data informada nao eh valida',
          [{ text: 'Ok', style: 'cancel' }],
          { cancelable: false },
        );
      }
    }
  };

  return (
    <Item>
      <Label style={styles.label}>{label}</Label>
      <Input
        value={dd}
        placeholder="DD"
        style={styles.input}
        placeholderTextColor={Colours.PLACEHOLDER_LIGHT}
        keyboardType="number-pad"
        maxLength={2}
        onChangeText={text => {
          if (text >= 0 && text <= 31) {
            setDD(text);
            checkDate(text, mm, yyyy);
          }
        }}
      />
      <Input
        value={mm}
        style={styles.input}
        placeholderTextColor={Colours.PLACEHOLDER_LIGHT}
        placeholder="MM"
        keyboardType="number-pad"
        maxLength={2}
        onChangeText={text => {
          if (text >= 0 && text <= 12) {
            setMM(text);
            checkDate(dd, text, yyyy);
          }
        }}
      />
      <Input
        value={yyyy}
        style={styles.input}
        placeholderTextColor={Colours.PLACEHOLDER_LIGHT}
        placeholder="AAAA"
        keyboardType="number-pad"
        maxLength={4}
        onChangeText={text => {
          setYYYY(text);
          checkDate(dd, mm, text);
        }}
      />
    </Item>
  );
};

const styles = StyleSheet.create({
  label: {
    width: 90,
    color: Colours.ORANGE,
  },
  input: {
    color: Colours.DARK_BLUE,
  },
});
export default DateField;
