import React from 'react';
import { View, StyleSheet, Image, TouchableHighlight } from 'react-native';

const ProgressiveImage = props => {
  const replaceSource = source => {
    if (!source.uri) {
      return source;
    }

    return {
      uri: source.uri.replace(
        's3heardboutbucket1.s3.amazonaws.com',
        'd20enyijjq54d2.cloudfront.net',
      ),
    };
  };

  return (
    <View style={[props.style, styles.container]} onTouchStart={props.onPress}>
      <Image
        style={[props.style, styles.imageOverlay]}
        source={replaceSource(props.source)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  imageOverlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    marginRight: 0,
    marginLeft: 0,
  },
  container: {
    backgroundColor: '#e1e4e8',
  },
});

export default ProgressiveImage;
