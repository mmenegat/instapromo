import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Colours } from '../styles/Styles';

const Divider = () => (
  <View style={styles.container}>
    <View style={styles.dividerLine} />
  </View>
);

export default Divider;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
  },
  dividerLine: {
    backgroundColor: Colours.DARK_BLUE,
    height: 1,
    flex: 1,
    alignSelf: 'center',
    marginTop: 3,
    marginLeft: 10,
    marginRight: 10,
  },
});
