import React from 'react';
import { Platform, StatusBar } from 'react-native';
import {
  createStackNavigator,
  createBottomTabNavigator,
  createSwitchNavigator,
} from 'react-navigation';

import Login from './pages/auth/login';
import Offers from './pages/offers/Offers';
import Profile from './pages/profile/Profile';
import Vouchers from './pages/voucher/Voucher';
import History from './pages/history/History';
import { Icon } from 'native-base';
import ProfileOptions from './pages/profile/ProfileOptions';
import UpdateUserName from './pages/profile/UpdateUserName';
import { Colours } from './styles/Styles';

const iconSize = 30;

const profileIcon = tintColor => (
  <Icon
    style={{ color: tintColor, fontSize: iconSize }}
    type="MaterialIcons"
    name="person"
  />
);
const historyIcon = tintColor => (
  <Icon
    style={{ color: tintColor, fontSize: iconSize }}
    type="MaterialIcons"
    name="history"
  />
);
const offersIcon = tintColor => {
  return (
    <Icon
      style={{ color: tintColor, fontSize: iconSize }}
      type="MaterialIcons"
      name="local-offer"
    />
  );
};
const vouchersIcon = tintColor => (
  <Icon style={{ color: tintColor, fontSize: iconSize }} name="wallet" />
);

export const SignedOut = createStackNavigator({
  SignUp: {
    screen: Login,
    navigationOptions: {
      header: null,
    },
  },
});

export const UpdateUsernameScreen = createStackNavigator({
  SignUp: {
    screen: UpdateUserName,
    navigationOptions: {
      header: null,
    },
  },
});

export const ProfileScreen = createStackNavigator({
  SignUp: {
    screen: Profile,
    navigationOptions: {
      header: null,
    },
  },
});

const navigationOptions = (label, icon) => ({
  tabBarLabel: label,
  tabBarIcon: ({ tintColor }) => icon(tintColor),
  tabBarOptions: {
    activeTintColor: Colours.ORANGE,
    inactiveTintColor: Colours.DARK_BLUE,
    labelStyle: {
      fontWeight: 'bold',
    },
  },
});

const SignedIn = createBottomTabNavigator(
  {
    Offers: {
      screen: Offers,
      navigationOptions: navigationOptions('Ofertas', offersIcon),
    },
    Vouchers: {
      screen: Vouchers,
      navigationOptions: navigationOptions('Vouchers', vouchersIcon),
    },
    History: {
      screen: History,
      navigationOptions: navigationOptions('Historico', historyIcon),
    },
    Profile: {
      screen: ProfileOptions,
      navigationOptions: navigationOptions('Perfil', profileIcon),
    },
  },
  {
    tabBarOptions: {
      style: {
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
      },
    },
  },
);

export const createRootNavigator = (signedIn = false) => {
  return createSwitchNavigator(
    {
      SignedIn: {
        screen: SignedIn,
      },
      Profile: {
        screen: ProfileScreen,
      },
      UpdateUsername: {
        screen: UpdateUsernameScreen,
      },
      SignedOut: {
        screen: SignedOut,
      },
    },
    {
      initialRouteName: signedIn ? 'SignedIn' : 'SignedOut',
    },
  );
};
