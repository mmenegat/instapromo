export const Colours = {
  DARK_BLUE: '#013183',
  WHITE: '#FFFFFF',
  ORANGE: '#FD7D00',
  RED: '#D60303',
  PLACEHOLDER: 'rgba(255, 255, 255, 0.5)',
  PLACEHOLDER_LIGHT: 'rgba(1, 49, 131, 0.5)',
  ICON: '#B7B7B7',
};
