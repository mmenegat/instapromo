import React, {useState } from 'react';
import {Platform, View, Alert} from 'react-native'
import { Button, Form, Item, Textarea, Input, Label, Container , Header, Left, Body, Right, Icon, Text} from 'native-base';
import { callAPI } from '../auth/AwsAPI';
import { getUserId } from '../auth/auth';
import Loader from '../../components/Loader';


const ReportProblem = (props) => {

    const {item, onGoBack, onSuccess} = props
    const [os, setOs] = useState(Platform.OS)
    const [version, setVersion] = useState(Platform.Version)
    const [brand, setBrand] = useState(os === "ios" ? "Apple" : "")
    const [model, setModel] = useState("")
    const [description, setDescription] = useState("")
    const [isLoading, setIsLoading] = useState(false)

    const onSubmit = () => {

        if(!(brand > "") || !(model > "") || !(description > "")){
            Alert.alert(
                'Erro', 'Marca, Modelo e Descricao devem ser informados para prosseguir!',
                [{text: 'Ok', style: 'cancel'}],
                {cancelable: false},
              );  
        } else{

            setIsLoading(true)
            getUserId()
            .then(userId => {
            
            let problem = { 
                            promotionId: item.offer.promotionId,
                            userId,  
                            osp: os,
                            versionp : version,
                            brand,
                            model,
                            description
                            }  
        
                
            callAPI('post', "reportProblem", {body : problem}) 
            .then(response => {        
                setIsLoading(false)

                Alert.alert(
                    'Alerta', 'Problema Reportado com sucesso',
                    [{text: 'Ok', style: 'cancel', onPress: () => onSuccess()}],
                    {cancelable: false},
                );
                
            })
            .catch(error => {
                setIsLoading(false)
            })
        
            })
            .catch(err =>  setIsLoading(false) );   
        }

    }

    return (
        <Container>
         <Header>
            <Left>
            <Button transparent onPress={onGoBack}>
                <Icon name='arrow-back' />
                <Text> Voltar</Text>
            </Button>
            </Left>
            <Right>
            <Button transparent onPress={onSubmit}>
                <Text>Reportar Problema </Text>
                <Icon name='bug' />
            </Button>
            </Right>
        </Header>   
        {isLoading ?       
            <Loader isLoading={isLoading} />
        :
            <React.Fragment>
            <Form>
                <Item>
                <Label style={{ width: 90 }}>OS</Label>
                <Input value={os} editable={false}/>
                </Item>
                <Item>
                <Label style={{ width: 90 }}>Versao</Label>
                <Input value={version} editable={false}/>
                </Item>

                <Item >
                <Label style={{ width: 90 }}>Marca</Label>
                <Input value={brand} editable={os != "ios"} onChangeText={text => {
                            setBrand(text) 
                }}/>
                </Item>

                <Item>
                <Label style={{ width: 90 }}>Modelo</Label>
                <Input value={model} placeholder="ex. Galaxy S20" onChangeText={text => {
                setModel(text) }}/>
                </Item>

                <Item style={{ marginBottom: 10 }}>
                <Textarea style={{ width: "95%" }} rowSpan={6} bordered value={description} placeholder="Detalhe o seu problema..." onChangeText={text => {
                setDescription(text) }}/>
                </Item>
            </Form>
            </React.Fragment>}
        </Container>
    )



}
export default ReportProblem