import React, { useState } from 'react';
import Loader from '../../components/Loader';
import { Container, Content } from 'native-base';
import OfferItem from './OfferItem';
import ReportProblem from './ReportProblem';
import { removeInTransitPromotion } from '../auth/auth';
import FormHeader from '../../components/FormHeader';
import { ConfirmMessage, ShowMessage } from '../../components/Modal';

const OfferList = props => {
  let { goBack, item } = props;
  const [isSharing, setIsSharing] = useState(false);
  const [isReportProblem, setIsReportProblem] = useState(false);
  const [currentItem, setCurremtItem] = useState({});
  const [confirmMessage, setConfirmMessage] = useState({
    isOpen: false,
    title: '',
    description: '',
    onCancel: closeMessage,
  });
  const [message, setMessage] = useState({
    isOpen: false,
    title: '',
    description: '',
    onClose: closeMessage,
  });

  const closeMessage = () => {
    setConfirmMessage({
      isOpen: false,
      title: '',
      description: '',
      onCancel: closeMessage,
    });
    setMessage({
      isOpen: false,
      title: '',
      description: '',
      onCancel: closeMessage,
    });
  };

  const reportProblem = item => {
    setIsReportProblem(true);
    setCurremtItem(item);
  };

  const getOfferItems = () => {
    let list = [];

    for (let i = 0; i < item.offers.length; i++) {
      let _offer = item.offers[i];
      let obj = { companyId: item.companyId, offer: _offer };

      list.push(
        <OfferItem
          item={obj}
          onGoBack={goBack}
          reportProblem={reportProblem}
          setConfirmMessage={setConfirmMessage}
          setMessage={setMessage}
          closeMessage={closeMessage}
          setIsSharing={e => setIsSharing(e)}
        />,
      );
    }
    return list;
  };

  if (isReportProblem) {
    return (
      <ReportProblem
        onSuccess={() => {
          removeInTransitPromotion(currentItem.offer.promotionId);
          setIsReportProblem(false);
        }}
        item={currentItem}
        onGoBack={() => setIsReportProblem(false)}
      />
    );
  }

  console.log('item ----', item);

  return (
    <Container>
      <ConfirmMessage {...confirmMessage} />
      <ShowMessage {...message} />
      <FormHeader title={item.establishment} onPressBack={goBack} />
      <Loader isLoading={isSharing} />
      <Content>{getOfferItems()}</Content>
    </Container>
  );
};
export default OfferList;
