import React, { useState, useEffect } from 'react';
import {
  ScrollView,
  View,
  RefreshControl,
  StyleSheet,
  Platform,
  Image,
} from 'react-native';
import { callAPI } from '../auth/AwsAPI';
import Loader from '../../components/Loader';

import { Container, Card, CardItem, Text, Badge } from 'native-base';

import { getUserHistory, getUserVouchers } from '../auth/auth';
import OfferList from './OfferList';
import FormHeader from '../../components/FormHeader';
import { Colours } from '../../styles/Styles';
import Divider from '../../components/Divider';
import ProgressiveImage from '../../components/ProgressiveImage';

const Offers = props => {
  const [offersList, setoffersList] = useState([]);
  const [hasLoaded, setHasLoaded] = useState(false);
  const [viewType, setViewType] = useState('list');
  const [selectedOffer, setSelectedOffer] = useState(-1);
  const [refreshing, setRefreshing] = useState(false);
  const [isRefreshingAfterPost, setIsRefreshingAfterPost] = useState(false);
  const [offerResponse, setOfferResponse] = useState({});
  let voucherList = [];
  let historyList = [];

  useEffect(() => {
    if (viewType === 'list') {
      setIsRefreshingAfterPost(true);
      getUserVouchers()
        .then(response => {
          voucherList = response;
        })
        .finally(() => {
          getUserHistory()
            .then(response => {
              historyList = response;
            })
            .finally(() => {
              processResponse(offerResponse);
              setIsRefreshingAfterPost(false);
            });
        });
    }
  }, [viewType]);

  const onRefresh = React.useCallback(() => {
    setHasLoaded(false);
  }, [refreshing]);

  const verifyVoucherUtilization = promoId => {
    for (let i = 0; i < voucherList.length; i++) {
      let voucher = voucherList[i];

      if (voucher.promoId === promoId) {
        return true;
      }
    }

    for (let i = 0; i < historyList.length; i++) {
      let voucher = historyList[i];

      if (voucher.promoId === promoId) {
        return true;
      }
    }

    return false;
  };

  const processResponse = response => {
    let _offersList = [];
    let companies = [];

    let offerKey = 0;

    for (let i = 0; i < response.Items.length; i++) {
      let item = response.Items[i];
      let company = JSON.parse(item.company);

      let _offer = {
        img: { uri: item.resourceUrl },
        name: item.promoName,
        startDate: item.promotionStartDate,
        endDate: item.promotionEndDate,
        voucherStart: item.voucherStartDate,
        voucherEndDate: item.voucherEndDate,
        description: item.description,
        rules: item.prules,
        hasBeenUsed: verifyVoucherUtilization(item.id),
        promotionId: item.id,
      };

      if (companies.includes(item.companyId)) {
        let idx = companies.indexOf(item.companyId);
        _offersList[idx].offers.push(_offer);
        _offersList[idx].totOf += 1;
        _offersList[idx].totOfAvail += _offer.hasBeenUsed ? 0 : 1;
      } else {
        let _companyOffer = {
          key: offerKey,
          establishment: company.companyName,
          sector: company.interests,
          image: { uri: company.logo },
          companyId: item.companyId,
          offers: [_offer],
          totOf: 1,
          totOfAvail: _offer.hasBeenUsed ? 0 : 1,
        };

        _offersList.push(_companyOffer);
        companies.push(item.companyId);
        offerKey++;
      }
    }

    setoffersList(_offersList);
  };

  if (!hasLoaded) {
    getUserVouchers()
      .then(response => {
        voucherList = response;
      })
      .finally(() => {
        getUserHistory()
          .then(response => {
            historyList = response;
          })
          .finally(() => {
            callAPI('get', 'getPromotionsByRegion', '')
              .then(response => {
                setOfferResponse(response);
                processResponse(response);
                setHasLoaded(true);
              })
              .catch(error => {
                console.log('error', error);
                setHasLoaded(true);
              });
          });
      });
  }

  if (viewType === 'offer') {
    let item = offersList[selectedOffer];

    const goBack = () => setViewType('list');

    return <OfferList goBack={goBack} item={item} {...props} />;
  }

  return (
    <Container>
      <FormHeader title="Promoções" onRefresh={onRefresh}/>
      <Loader isLoading={!hasLoaded && !isRefreshingAfterPost} />
      <ScrollView
        contentContainerStyle={{ paddingVertical: 20 }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {hasLoaded &&
          offersList.map(
            ({ establishment, sector, image, key, totOf, totOfAvail }) => {
              return (
                <Card transparent style={styles.card}>
                  <CardItem cardBody>
                    <ProgressiveImage
                      source={image}
                      style={styles.cardImage}
                      onPress={() => {
                        setSelectedOffer(key);
                        setViewType('offer');
                      }}
                    />
                  </CardItem>

                  <View style={styles.cardDetails}>
                    <Text style={styles.headline}>{establishment}</Text>
                    <Text style={styles.text}>{totOfAvail} Disponiveis</Text>
                    <Badge style={styles.badge}>
                      <Text>{sector}</Text>
                    </Badge>
                  </View>
                  <Divider />
                </Card>
              );
            },
          )}
      </ScrollView>
    </Container>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingTop: Platform.OS === 'ios' ? 25 : 0,
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    marginRight: 10,
    marginLeft: 10,
  },
  headline: {
    fontWeight: 'bold',
    fontSize: 20,
    color: Colours.ORANGE,
    marginTop: 7,
  },
  text: {
    fontSize: 15,
    marginTop: 7,
    color: Colours.DARK_BLUE,
  },
  cardImage: {
    height: 150,
    width: null,
    flex: 1,
    borderRadius: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  cardDetails: {
    marginLeft: 10,
    marginRight: 10,
  },
  badge: {
    backgroundColor: Colours.DARK_BLUE,
    marginTop: 7,
    marginBottom: 7,
  },
});

export default Offers;
