import React, { useState, useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  BackHandler,
  Image,
  StyleSheet,
} from 'react-native';
import { callAPI } from '../auth/AwsAPI';
import {
  getUserId,
  addInTransitPromotion,
  removeInTransitPromotion,
  getInTransitPromotions,
  getUserVouchers,
  setUserVouchers,
} from '../auth/auth';
import { formatDate } from '../../util/util';
import ImgToBase64 from 'react-native-image-base64';
import RNLegitStoryShare from 'react-native-legit-story-share';
import ReportProblem from './ReportProblem';
import { Colours } from '../../styles/Styles';
import Divider from '../../components/Divider';

import { Card, CardItem, Text, Button, Icon } from 'native-base';
import ProgressiveImage from '../../components/ProgressiveImage';

const OfferItem = props => {
  let {
    item,
    onGoBack,
    setIsSharing,
    reportProblem,
    setMessage,
    setConfirmMessage,
    closeMessage,
  } = props;

  const [isOpenDescription, setIsOpenDescription] = useState(true);
  const [isOpenRules, setIsOpenRules] = useState(false);
  const [isOpenValid, setIsOpenValid] = useState(false);
  const [isInTransit, setIsInTransint] = useState(null);
  const [hasShared, setHasShared] = useState(item.offer.hasBeenUsed);

  /* if(isInTransit === null){
        getInTransitPromotions()
        .then(result => {
            let filtered = result.filter(value => value === item.offer.promotionId)
            setIsInTransint(filtered.length > 0)
        })
        .catch(err => setIsInTransint(false))
    }*/
  const renderBackButton = () => {
    onGoBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', renderBackButton);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', renderBackButton);
    };
  }, [renderBackButton]);

  useEffect(() => {
    getInTransitPromotions()
      .then(result => {
        let filtered = result.filter(value => value === item.offer.promotionId);
        setIsInTransint(filtered.length > 0);
      })
      .catch(err => setIsInTransint(false));
  });

  const goBack = () => {
    onGoBack();
  };

  const renderItem = (title, content, isOpen, setIsOpen) => {
    return (
      <TouchableOpacity style={styles.item} onPress={() => setIsOpen(!isOpen)}>
        <View style={{ flexDirection: 'row' }}>
          <Icon
            style={{ color: Colours.DARK_BLUE, marginLeft: 10 }}
            name={isOpen ? 'arrow-up' : 'arrow-down'}
          />
          <Text style={styles.headlineItem}> {title}</Text>
        </View>

        {isOpen ? <Text style={styles.textItem}>{content}</Text> : null}
        <Divider style={{ height: 2, marginTop: 15 }} />
      </TouchableOpacity>
    );
  };

  const checkVoucherForPromotion = cb => {
    getUserId()
      .then(userId => {
        let voucher = {
          companyId: item.companyId,
          promotionId: item.offer.promotionId,
          userId,
        };

        callAPI('post', 'verifyCreateVoucher', { body: voucher })
          .then(response => {
            console.log(response);
            cb(null, response);
          })
          .catch(error => {
            console.log(error.response);
            cb(error, null);
          });
      })
      .catch(err => cb(err, null));
  };

  const sendVoucherForValidation = () => {
    setIsSharing(true);

    getUserId()
      .then(userId => {
        let voucher = {
          companyId: item.companyId,
          promotionId: item.offer.promotionId,
          userId,
          voucherStartDate: item.offer.voucherStart,
          voucherEndDate: item.offer.voucherEndDate,
          hasBeenConfirmed: 'false',
        };

        callAPI('post', 'createVoucher', { body: voucher })
          .then(response => {
            setIsSharing(false);
            removeInTransitPromotion(item.offer.promotionId);
            setIsInTransint(false);

            setMessage({
              isOpen: true,
              title: 'Sucesso',
              description:
                'Compartilhamento enviado para validação com sucesso!',
              onClose: () => {
                closeMessage();
                getUserVouchers()
                  .then(voucherList => {
                    voucherList.push({
                      confirmed: false,
                      id: '',
                      img: { uri: item.offer.img.uri },
                      endDate: item.offer.voucherEndDate,
                      key: voucherList.length + 1,
                      name: item.offer.name,
                      promoId: item.offer.promotionId,
                      startDate: item.offer.voucherStart,
                    });
                    setUserVouchers(voucherList);
                  })
                  .catch(e => console.log('C'))
                  .finally(() => setHasShared(true));
              },
            });
            return;
          })
          .catch(error => {
            console.log(error.response);
            setIsSharing(false);
            return;
          });
      })
      .catch(err => {
        setIsSharing(false);
        return;
      });
  };

  const shareWithInstagram = item => {
    closeMessage();
    setIsSharing(true);

    RNLegitStoryShare.isInstagramAvailable()
      .then(isAvailable => {
       /* if (!isAvailable) {
          setMessage({
            isOpen: true,
            title: 'Instagram',
            description:
              'Por favor instale o instagram para prosseguir com o compartilhamento',
            onClose: closeMessage,
          });

          setIsSharing(false);
          return;
        }*/

        checkVoucherForPromotion((err, data) => {
          if (err) {
            setIsSharing(false);
            setMessage({
              isOpen: true,
              title: 'Promocao compartilhada anteriormente',
              description:
                'Verifique seus vouchers ou tente novamente mais tarde',
              onClose: closeMessage,
            });
            return;
          }

          ImgToBase64.getBase64String(item.offer.img.uri)
            .then(base64String => {
              RNLegitStoryShare.shareToInstagram({
                type: RNLegitStoryShare.BASE64, // or RNLegitStoryShare.FILE
                backgroundAsset: 'data:image/jpeg;base64,' + base64String,
              });

              addInTransitPromotion(item.offer.promotionId);
              setIsInTransint(true);
              setIsSharing(false);
            })
            .catch(e => console.log(e));
        }).catch(err => console.log('no'));
      })
      .catch(err => console.log('no'));
  };

  const onClickSharePromotion = () =>
    setConfirmMessage({
      isOpen: true,
      title: 'Instagram',
      description:
        'Você será redirecionado para os stories do Instagram. Compartilhe marcando @heardbout',
      onConfirm: () => shareWithInstagram(item),
      onCancel: closeMessage,
    });

  const onClickValidatePromotion = () =>
    setConfirmMessage({
      isOpen: true,
      title: 'Solicitar Voucher',
      description:
        'Voucher será garantido após validação do compartilhamento pela equipe do heardbout. Deseja prosseguir?',
      onConfirm: () => {
        closeMessage();
        sendVoucherForValidation(item);
      },
      onCancel: closeMessage,
    });

  const onClickCancelSharing = () =>
    setConfirmMessage({
      isOpen: true,
      title: 'Cancelar compartilhamento?',
      description:
        'Ao cancelar o compartilhamento a promoção voltará ao status inicial permitindo nova postagem',
      onConfirm: () => {
        closeMessage();
        removeInTransitPromotion(item.offer.promotionId);
        setIsInTransint(false);
      },
      onCancel: closeMessage,
    });

  return (
    <Card style={styles.card} transparent>
      <CardItem cardBody>
        <ProgressiveImage
          source={item.offer.img}
          style={styles.cardImage}
          key={item.key}
        />
      </CardItem>

      {!hasShared ? (
        <View style={styles.cardDetails}>
          <Text style={styles.headline}>Promoção válida até</Text>
          <Text style={styles.text}>
            {formatDate(item.offer.voucherEndDate)}
          </Text>
          {renderItem(
            'Descricao',
            item.offer.description,
            isOpenDescription,
            setIsOpenDescription,
          )}
          {renderItem(
            'Regras da Promocao',
            item.offer.rules,
            isOpenRules,
            setIsOpenRules,
          )}
          {renderItem(
            'Validade do Voucher',
            formatDate(item.offer.voucherStart) +
              ' a ' +
              formatDate(item.offer.voucherEndDate),
            isOpenValid,
            setIsOpenValid,
          )}

          {isInTransit === false ? (
            <Button
              style={styles.actionButton}
              block
              onPress={onClickSharePromotion}>
              <Text>Compartilhar Promocao</Text>
            </Button>
          ) : (
            <>
              <Button
                style={styles.actionButton}
                block
                onPress={onClickValidatePromotion}>
                <Text>Solicitar Voucher</Text>
              </Button>

             
              <Button
                style={styles.actionButtonLight}
                block
                onPress={onClickCancelSharing}>
                <Text style={styles.textButtonLight}>Cancelar Postagem (Repostar)</Text>
              </Button>
            </>
          )}
        </View>
      ) : (
        <View style={styles.cardDetails}>
          <Text style={styles.headline}>Promoção ja compartilhada!</Text>
          <Text style={styles.text}>
            Verifique o status de sua postagem na aba vouchers.
          </Text>
        </View>
      )}
      <Divider />
    </Card>
  );

  /*                      <Button block danger style={{marginTop: 10}} onPress={() => {reportProblem(item)}}>
                            <Text>Reportar Problema</Text>
                          </Button>
  */
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingTop: Platform.OS === 'ios' ? 25 : 0,
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 10,
  },
  headline: {
    fontWeight: 'bold',
    color: Colours.DARK_BLUE,
    marginTop: 7,
  },
  text: {
    marginTop: 7,
    color: Colours.DARK_BLUE,
  },
  cardImage: {
    height: 150,
    width: null,
    flex: 1,
    borderRadius: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  cardDetails: {
    marginLeft: 10,
    marginRight: 10,
  },
  badge: {
    backgroundColor: Colours.DARK_BLUE,
    marginTop: 7,
    marginBottom: 7,
  },
  item: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 15,
  },
  headlineItem: {
    fontWeight: 'bold',
    color: Colours.DARK_BLUE,
    marginTop: 7,
  },
  textItem: {
    marginTop: 7,
    marginLeft: 10,
    marginBottom: 10,
    color: Colours.DARK_BLUE,
  },
  actionButton: {
    marginLeft: 25,
    marginRight: 30,
    borderRadius: 10,
    backgroundColor: Colours.ORANGE,
    marginBottom: 20,
    marginTop: 20,
  },
  actionButtonLight: {
    marginLeft: 25,
    marginRight: 30,
    borderRadius: 10,
    backgroundColor: Colours.WHITE,
    borderWidth: 1,
    borderColor: Colours.DARK_BLUE,
    marginBottom: 20,
  },
  textButtonLight: {
    color: Colours.DARK_BLUE,
  },
});
export default OfferItem;
