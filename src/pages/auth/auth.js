import { AsyncStorage } from 'react-native';

export const USER_KEY = 'userToken';
export const INSTA_TOKEN = 'instaToken';
export const AWS_REGISTRED = 'awsRegistred';
export const AWS_CREDENTIALS = 'awsCredentials';
export const USER_ID = 'userId';
export const USER_NAME = 'username'
export const PROFILE = "profile"
export const VOUCHERS = "vouchers"
export const HISTORY = "vouchersHistory"
export const IN_TRANSIT_PROMOTION = "promotionsInTransit"

export const setInstaToken = insta_token => {
  AsyncStorage.setItem(INSTA_TOKEN, insta_token);
};

export const getInstaToken = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(INSTA_TOKEN)
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};

export const setAwsRegistred = isRegistred => {
  AsyncStorage.setItem(AWS_REGISTRED, isRegistred);
};

export const isAwsRegistred = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(AWS_REGISTRED)
      .then(res => {
        if (res !== null) {
          resolve(res);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};

export const setUserId = userId => {
  //console.log("userrrrrrrrrrrrrrrr", userId)
  AsyncStorage.setItem(USER_ID, userId + "");
};

export const setUserName = username => {
  AsyncStorage.setItem(USER_NAME, username);
};

export const setProfilePhoto = url => {
  AsyncStorage.setItem(PROFILE, url);
}

export const setUserVouchers = vouchers => {
  AsyncStorage.setItem(VOUCHERS, JSON.stringify(vouchers))
}

export const setUserHistory = vouchers => {
  AsyncStorage.setItem(HISTORY, JSON.stringify(vouchers))
}

export const addInTransitPromotion = promotionID => {
  getInTransitPromotions()
  .then(promotions => {
    promotions.push(promotionID)
    AsyncStorage.setItem(IN_TRANSIT_PROMOTION, JSON.stringify(promotions))
  })
}

export const removeInTransitPromotion = promotionID => {
  getInTransitPromotions()
  .then(promotions => { 
    let filtered = promotions.filter((value) =>{
      return value != promotionID;
    });
    AsyncStorage.setItem(IN_TRANSIT_PROMOTION, JSON.stringify(filtered))
  })
}

export const getInTransitPromotions = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(IN_TRANSIT_PROMOTION)
      .then(res => {
        if (res > "") {
          resolve(JSON.parse(res));
        } else {
          resolve([]);
        }
      })
      .catch(err => reject(err));
  });
}

export const getUserId = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(USER_ID)
      .then(res => {
        if (res !== null) {
          resolve(res);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};

export const getUsername = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(USER_NAME)
      .then(res => {
        if (res !== null) {
          resolve(res);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};

export const getProfilePhoto = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(PROFILE)
      .then(res => {
        if (res !== null) {
          resolve(res);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};

export const getUserVouchers = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(VOUCHERS)
      .then(res => {
        if (res > "") {
          resolve(JSON.parse(res));
        } else {
          resolve([]);
        }
      })
      .catch(err => reject(err));
  });
};


export const getUserHistory = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(HISTORY)
      .then(res => {
        if (res > "") {
          resolve(JSON.parse(res));
        } else {
          resolve([]);
        }
      })
      .catch(err => reject(err));
  });
};

export const onSignIn = user_token => {
  return new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.setItem(USER_KEY, user_token, () => {
        resolve(true);
      });
    } catch (err) {
      reject(err);
    }
  });
};

export const onSignOut = async () => await AsyncStorage.removeItem(USER_KEY);

export const isSignedIn = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(USER_KEY)
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};

export const getAuthIn = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(USER_KEY)
      .then(res => {
        if (res !== null) {
          resolve(res);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};
