import React, { Component, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Text,
  Button,
  Container,
  Content,
  Form,
  Item,
  Input,
  Header,
  Left,
  Icon,
  Toast,
  Body,
  Right,
} from 'native-base';
import { setUserId, setUserName, setProfilePhoto } from './auth';
import { Auth } from 'aws-amplify';
import errorMap from './cognitoErrorMap';
import Loader from '../../components/Loader';
import { callAPI } from './AwsAPI';
import axios from 'axios';
import { Colours } from '../../styles/Styles';
import { ShowMessage } from '../../components/Modal';

const UserLogin = props => {
  const [view, setView] = props.isRegister
    ? useState('register')
    : useState('login');
  const [title, setTitle] = props.isRegister
    ? useState('Registre-se')
    : useState('Login');
  const [email, setEmail] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [password, setPassword] = useState('');
  const [code, setCode] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [message, setMessage] = useState({
    isOpen: false,
    title: '',
    description: '',
  });

  const onPressBack = () => {
    switch (view) {
      case 'recover':
        setView('login');
        setTitle('Login');
        break;
      case 'resetPassword':
        setView('recover');
        setTitle('Recuperar a Senha');
        break;
      case 'registerCode':
        props.isRegister ? setView('login') : setView('register');
        break;
      default:
        props.onBack();
    }
  };

  const renderError = e => {
    setIsLoading(false);

    setMessage({
      isOpen: true,
      title: 'Ocorreu um erro',
      description: errorMap(e.code),
    });
  };

  const registerFirstStep = async () => {
    try {
      setIsLoading(true);
      await Auth.signUp({
        email: email,
        username: email,
        password: password,
      });

      Toast.show({
        text: 'Email enviado para ' + email + ' contendo codigo de verificacao',
        buttonText: 'ok',
        type: 'success',
        duration: 3000,
      });

      setIsLoading(false);
      setView('registerCode');
    } catch (e) {
      renderError(e);
    }
  };

  const validateCode = async () => {
    try {
      setIsLoading(true);
      await Auth.confirmSignUp(email, code);
      onLogin();
    } catch (e) {
      renderError(e);
    }
  };

  const resetPassword = async () => {
    try {
      setIsLoading(true);
      await Auth.forgotPasswordSubmit(email, code, password);
      onLogin();
    } catch (e) {
      renderError(e);
    }
  };

  const onPressRecoverPassword = async () => {
    if (view != 'recover') {
      setTitle('Recuperar a Senha');
      setView('recover');
      return;
    }

    if (!(email > '')) {
      setMessage({
        isOpen: true,
        title: 'Dados Invalidos',
        description: 'Por favor informe email para continuar.',
      });
      return;
    }

    try {
      setIsLoading(true);
      await Auth.forgotPassword(email);
      setIsLoading(false);
      setView('resetPassword');
      setMessage({
        isOpen: true,
        title: 'Codigo de Verificacao',
        description: 'Email enviado para ' + email,
      });
    } catch (e) {
      renderError(e);
    }
  };

  const onLogin = async () => {
    let hasSetUserName = false;

    if (!(email > '') || !(password > '')) {
      setMessage({
        isOpen: true,
        title: 'Dados Invalidos',
        description: 'Por favor informe email e senha para continuar.',
      });
      return;
    }

    try {
      setIsLoading(true);
      let auth = await Auth.signIn(email, password);

      setUserId(auth.attributes.sub);
      setUserName('');
      setProfilePhoto('');

      callAPI('post', 'getuserprofile', { body: { id: auth.attributes.sub } })
        .then(response => {
          if (response.Item) {
            let { instaUsername } = response.Item;
            setUserName(instaUsername);

            if (instaUsername > '') {
              axios
                .get('https://www.instagram.com/' + instaUsername + '/?__a=1')
                .then(result => {
                  setProfilePhoto(result.data.graphql.user.profile_pic_url);
                  setIsLoading(false);
                  props.onLogin();
                  return;
                })
                .catch(err => {
                  setIsLoading(false);
                  props.onLogin();
                  return;
                })
            } else {
              setIsLoading(false);
              props.onNewUser();
            }
          } else {
            setIsLoading(false);
            props.onNewUser();
          }
        })
        .catch(error => {
          setIsLoading(false);
          props.onNewUser();
        });
    } catch (e) {
      if (e.code === 'UserNotConfirmedException') {
        await Auth.resendSignUp(email);
        setIsLoading(false);
        setView('registerCode');

        setMessage({
          isOpen: true,
          title: 'Login nao confirmado',
          description:
            'Email enviado para ' +
            email +
            ' contendo novo codigo de verificacao!',
        });
      } else {
        renderError(e);
      }
    }
  };

  const renderHeader = () => (
    <>
      <Header style={styles.header}>
        <Left>
          <Button transparent onPress={onPressBack}>
            <Icon
              style={{ color: Colours.WHITE }}
              type="MaterialIcons"
              name="keyboard-backspace"
            />
          </Button>
        </Left>
        <Body>
        </Body>
      </Header>
    </>
  );

  const renderTitle = pTitle => (
    <>
      <Text style={styles.title}>{pTitle ? pTitle : title}</Text>

      <ShowMessage
        {...message}
        onClose={() => {
          setMessage({ isOpen: false, title: '', description: '' });
        }}
      />
    </>
  );

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Loader isLoading={true} />
      </View>
    );
  }

  if (view === 'registerCode') {
    return (
      <Container style={styles.mainContainer}>
        {renderHeader()}
        <Content contentContainerStyle={styles.content}>
          {renderTitle('Digite código enviado para o e-mail cadastrado')}
          <Form style={styles.form}>
            <Item>
              <Input
                placeholder="Codigo"
                style={styles.formItem}
                placeholderTextColor={Colours.PLACEHOLDER}
                value={code}
                onChangeText={text => setCode(text)}
              />
            </Item>
          </Form>
          <Button block style={styles.actionButton} onPress={validateCode}>
            <Text style={{ fontWeight: 'bold' }}>Enviar</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  if (view === 'resetPassword') {
    return (
      <Container style={styles.mainContainer}>
        {renderHeader()}
        <Content contentContainerStyle={styles.content}>
          {renderTitle()}
          <Form style={styles.form}>
            <Item>
              <Input
                style={styles.formItem}
                placeholderTextColor={Colours.PLACEHOLDER}
                placeholder="Codigo"
                value={code}
                onChangeText={text => setCode(text)}
              />
            </Item>
            <Item>
              <Input
                style={styles.formItem}
                placeholderTextColor={Colours.PLACEHOLDER}
                placeholder="Senha"
                value={password}
                secureTextEntry={true}
                onChangeText={text => setPassword(text)}
              />
            </Item>
            <Item error={confirmPassword != password && confirmPassword > ''}>
              <Input
                style={styles.formItem}
                placeholderTextColor={Colours.PLACEHOLDER}
                placeholder="Confirmar Senha"
                value={confirmPassword}
                secureTextEntry={true}
                onChangeText={text => setConfirmPassword(text)}
              />
            </Item>
          </Form>
          <Button block style={styles.actionButton} onPress={resetPassword}>
            <Text style={{ fontWeight: 'bold' }}>Confirmar Senha</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  return (
    <Container style={styles.mainContainer}>
      {renderHeader()}
      <Content contentContainerStyle={styles.content}>
        {renderTitle()}
        <Form style={styles.form}>
          <Item>
            <Input
              style={styles.formItem}
              placeholderTextColor={Colours.PLACEHOLDER}
              placeholder="E-mail"
              value={email}
              onChangeText={text => setEmail(text.trim())}
            />
          </Item>
          {view != 'recover' && (
            <Item>
              <Input
                style={styles.formItem}
                placeholderTextColor={Colours.PLACEHOLDER}
                placeholder="Senha"
                value={password}
                secureTextEntry={true}
                onChangeText={text => setPassword(text)}
              />
            </Item>
          )}
          {view === 'register' && (
            <Item error={confirmPassword != password && confirmPassword > ''}>
              <Input
                style={styles.formItem}
                placeholderTextColor={Colours.PLACEHOLDER}
                placeholder="Confirmar Senha"
                value={confirmPassword}
                secureTextEntry={true}
                onChangeText={text => setConfirmPassword(text)}
              />
            </Item>
          )}
        </Form>

        {view === 'login' && (
          <>
            <Button block style={styles.actionButton} onPress={onLogin}>
              <Text style={{ fontWeight: 'bold' }}>Entrar</Text>
            </Button>
            <Text style={styles.textOrange} onPress={onPressRecoverPassword}>
              Recuperar Senha
            </Text>
          </>
        )}

        {view === 'register' && (
          <>
            <Button
              block
              style={styles.actionButton}
              onPress={registerFirstStep}>
              <Text style={{ fontWeight: 'bold' }}>Enviar</Text>
            </Button>
          </>
        )}

        {view === 'recover' && (
          <Button
            block
            style={styles.actionButton}
            onPress={onPressRecoverPassword}>
            <Text style={{ fontWeight: 'bold' }}>Enviar</Text>
          </Button>
        )}
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colours.DARK_BLUE,
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0,
    borderBottomWidth: 0,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: Colours.DARK_BLUE,
  },
  content: {
    justifyContent: 'center',
    flex: 1,
    width: '100%',
    alignSelf: 'center',
  },
  title: {
    fontSize: 20,
    color: Colours.WHITE,
    marginLeft: 30,
    paddingBottom: 20,
  },
  form: {
    marginLeft: 10,
    marginRight: 30,
    paddingBottom: 20,
  },
  formItem: {
    color: Colours.WHITE,
  },
  actionButton: {
    marginLeft: 25,
    marginRight: 30,
    borderRadius: 10,
    backgroundColor: Colours.ORANGE,
    marginBottom: 20,
  },
  textOrange: {
    color: Colours.ORANGE,
    alignSelf: 'center',
  },
});

export default UserLogin;
