import { API, Auth } from 'aws-amplify';
//import axios from 'axios';
//import { getUserId } from './auth';

 /*const configureAmplify = token => {
    return new Promise((resolve, reject) => {
        Auth.federatedSignIn('developer', {
        token: token.Token,
        identity_id: token.IdentityId,
        })
        .then(cred => {
        return Auth.currentAuthenticatedUser();
        })
        .then(user => resolve(true))
        .catch(e => reject(e));
    });
};*/

/*const reauthenticate = () => {
    return new Promise((resolve, reject) => {
      getUserId()
      .then(userId => {
          axios.post('https://ewd0b22ib8.execute-api.us-east-1.amazonaws.com/dev/revalidate-token',
                    { user_id: userId })
  
        .then(response => {
            configureAmplify(response.data.aws)
            .then(res => resolve(true))
            .catch((error) => reject(error))        
        })
        .catch((error) => reject(error))
      })
      .catch((error) => reject(error))
    })
}*/

const callGetAPI = (apiName, body, authError) =>{
    return new Promise((resolve, reject) => {
        API.get(apiName, body, {})
        .then(response => {
            resolve(response)
        })
        .catch(error => {
          /* if(error.response.status === 403 && !authError){
               reauthenticate()
               .then((res) =>{
                  callGetAPI(apiName, body, true)
                  .then(response =>{
                      resolve(response)
                  })
                  .catch(error => reject(error))
               })
               .catch((error) => reject(error))
           } else {*/
               reject(error)
          // }
        });
    })    
}

const callPostAPI = (apiName, body, authError) =>{
console.log(apiName, body)

    return new Promise((resolve, reject) => {
        API.post(apiName, "",  body)
        .then(response => {
            resolve(response)
        })
        .catch(error => {
          /* if(error.response.status === 403 && !authError){
               reauthenticate()
               .then((res) =>{
                  callPostAPI(apiName, body, true)
                  .then(response =>{
                      resolve(response)
                  })
                  .catch(error => reject(error))
               })
               .catch((error) => reject(error))
           } else {*/
               reject(error)
          // }
        });
    })    
}



export const callAPI = (method, apiName, body) => {

    return new Promise((resolve, reject) => {
        if(method === "get"){
           callGetAPI(apiName, body, false)
           .then(res => resolve(res))
           .catch(err => reject(err))
        } else if (method === "post"){
            callPostAPI(apiName, body, false)
           .then(res => resolve(res))
           .catch(err => reject(err))
        } else{
            reject(new Error("not supported"))
        }
    })
};