export default function errorMap(errCode){
    switch(errCode){
        case "NotAuthorizedException" : return "Nome do usuario ou senha incorreto"
        case "UserNotFoundException" : return "Usuario nao cadastrado"
        case "CodeMismatchException" : return "Codigo de validacao invalido"
        case "InvalidParameterException" : return "Senha deve conter pelo menos: 6 caracteres"
        case "UsernameExistsException" : return "Ja existe usuario cadastrado com este email"
        case "InvalidPasswordException" : return "Senha deve conter pelo menos: 6 caracteres"
        default: return "Ocorre um erro interno. Tente novamente ou contate suporte"
    }
}