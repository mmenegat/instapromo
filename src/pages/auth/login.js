import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { Auth } from 'aws-amplify';
import axios from 'axios';

// import InstagramLogin from 'react-native-instagram-login';
import InstagramLogin from './instagram';
import { onSignIn, setUserId, setUserName, setProfilePhoto } from './auth';
import Loader from '../../components/Loader';
//import { SocialIcon } from 'react-native-elements';
import { Icon, Text, Button, Container } from 'native-base';
import UserLogin from './UserLogin';
import { Colours } from '../../styles/Styles';
//import ProgressiveImage from '../../components/ProgressiveImage';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      instaToken: '',
      isAuthenticatedWithAWS: 'false',
      errorAuth: '',
      apiMsg: '',
      isLoading: false,
      loginWithEmail: false,
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    Auth.currentAuthenticatedUser({ bypassCache: true })
      .then(user => {
        setUserId(user.attributes.sub);
        this.props.navigation.navigate('SignedIn');
      })
      .catch(err => {
        console.log(err);
      })
      .finally(() => this.setState({ isLoading: false }));
  }

  /*configureAmplify = async token => {
    await Auth.federatedSignIn('developer', {
      token: token.Token,
      identity_id: token.IdentityId,
    })
      .then(cred => {
        // If success, you will get the AWS credentials
        return Auth.currentAuthenticatedUser();
      })
      .then(user => {
        // If success, the user object you passed in Auth.federatedSignIn
        this.setState({ instaToken: 'autenticado com AWS' });
        // this.props.navigation.navigate('App');
      })
      .catch(e => {
        console.log(e);
        this.setState({ instaToken: e.stack.toString() });
      });
  };

  onSuccess = async instaToken => {
    this.setState({ failure: '', isLoading: true });
    instaToken = instaToken.replace('#_', '');
    this.setState({ instaToken });

    await axios
      .post(
        'https://xuxl80y0ua.execute-api.us-east-1.amazonaws.com/dev/validatecode',
        { code: instaToken },
      )
      .then(response => {
        this.setState({
          instaToken: JSON.stringify(response.data.instagram),
          isLoading: false,
        });
        this.configureAmplify(response.data.aws);
        setUserId(response.data.instagram.id);

        if (response.data.instagram.username === 'notavailable') {
          setUserName('');
          setProfilePhoto('');
          onSignIn(instaToken)
            .then(res => {
              this.props.navigation.navigate('UpdateUsername');
            })
            .catch(err => alert('An error occurred - onSignIn'));
          return;
        }

        setUserName(response.data.instagram.username);

        axios
          .get(
            'https://www.instagram.com/' +
            response.data.instagram.username +
            '/?__a=1',
          )
          .then(result => {
            setProfilePhoto(result.data.graphql.user.profile_pic_url);
          })
          .finally(() => {
            onSignIn(instaToken)
              .then(res => {
                this.props.navigation.navigate('Profile');
              })
              .catch(err => alert('An error occurred - onSignIn'));
            return;
          });
      })
      .catch(error => {
        alert(error.response.data);
        this.setState({ failure: JSON.stringify(error.response.data) });
        return;
      });
  };*/

  renderLogo = () => {
    return (
      <>
        <Image source={require('../../assets/app-logo.png')} />
      </>
    );
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Loader isLoading={true} />
        </View>
      );
    }

    let { loginWithEmail, register } = this.state;

    return (
      <Container>
        {loginWithEmail || register ? (
          <UserLogin
            isRegister={register}
            onNewUser={() => this.props.navigation.navigate('UpdateUsername')}
            onLogin={() => this.props.navigation.navigate('Profile')}
            onBack={() =>
              this.setState({ loginWithEmail: false, register: false })
            }
          />
        ) : (
            <Container style={styles.mainContainer}>
              {this.renderLogo()}

              <Button
                icon
                style={styles.loginWithEmail}
                onPress={() => this.setState({ loginWithEmail: true })}>
                <Icon name="mail" />
                <Text style={styles.loginWithEmailText}>Entrar com Email</Text>
              </Button>



              <View style={styles.registerContainer}>
                <Text style={styles.textWhite}>Não tem uma conta, </Text>
                <Text
                  style={styles.textOrange}
                  onPress={() => {
                    this.setState({ register: true });
                  }}>
                  Registre-se.
              </Text>
              </View>
            </Container>
          )}
      </Container>
    );
  }
}

/*
  <View style={styles.divider}>
              <View style={styles.dividerLine} />
              <Text style={styles.dividerText}> ou </Text>
              <View style={styles.dividerLine} />
            </View>

            <Button
              icon
              style={styles.socialIcon}
              onPress={() => this.instagramLogin.show()}>
              <Icon name="instagram" type="FontAwesome5" />
              <Text style={styles.loginWithEmailText}>
                Entrar com Instagram
              </Text>
            </Button>


                   <InstagramLogin
                ref={ref => (this.instagramLogin = ref)}
                appId="1272878013100253"
                redirectUrl="https://promoapp.rpxnow.com/instagram/callback"
                scopes={['user_profile']}
                onLoginSuccess={this.onSuccess}
                onLoginFailure={data => console.log(data)}
              />
            */

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colours.DARK_BLUE,
  },
  loginWithEmail: {
    width: '85%',
    height: 50,
    backgroundColor: Colours.DARK_BLUE,
    borderColor: Colours.WHITE,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 200,
    borderRadius: 10,
  },
  socialIcon: {
    width: '85%',
    height: 50,
    backgroundColor: Colours.ORANGE,
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 10,
  },
  loginWithEmailText: {
    fontSize: 14,
    padding: 0,
  },
  divider: {
    width: '85%',
    margin: 10,
    flexDirection: 'row',
  },
  dividerLine: {
    backgroundColor: Colours.WHITE,
    height: 1,
    flex: 1,
    alignSelf: 'center',
  },
  dividerText: {
    alignSelf: 'center',
    paddingHorizontal: 5,
    color: Colours.WHITE,
  },
  registerContainer: {
    width: '85%',
    margin: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textWhite: {
    color: Colours.WHITE,
  },
  textOrange: {
    color: Colours.ORANGE,
  },
});

// { uri: `https://api.instagram.com/oauth/authorize?app_id=${appId}&redirect_uri='https://google.com'&response_type="code"&scope=user_profile` }
