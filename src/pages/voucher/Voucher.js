import React, { useState, useEffect } from 'react';
import { getUserId, getUserVouchers, setUserVouchers } from '../auth/auth';
import { callAPI } from '../auth/AwsAPI';
import Loader from '../../components/Loader';
import {
  ScrollView,
  View,
  StyleSheet,
  RefreshControl,
  BackHandler,
  Platform,
  TouchableOpacity,
  Image,
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import { formatDate } from '../../util/util';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Badge,
} from 'native-base';
import FormHeader from '../../components/FormHeader';
import Divider from '../../components/Divider';
import { Colours } from '../../styles/Styles';
import ProgressiveImage from '../../components/ProgressiveImage';

let selectedOffer;

const Voucher = () => {
  const [voucherList, setVoucherList] = useState([]);
  const [constructor, setConstructor] = useState(true);
  const [hasLoaded, setHasLoaded] = useState(false);
  const [renderQRCode, setRenderQRCode] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [isOpenDescription, setIsOpenDescription] = useState(true);
  const [isOpenRules, setIsOpenRules] = useState(false);
  const [isOpenValid, setIsOpenValid] = useState(false);

  const renderBackButton = () => {
    setRenderQRCode(false);
    return true;
  };

  const goBack = () => {
    setRenderQRCode(false);
  };

  useEffect(() => {
    if (renderQRCode) {
      BackHandler.addEventListener('hardwareBackPress', renderBackButton);
    }

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', renderBackButton);
    };
  }, [renderBackButton]);

  const onRefresh = React.useCallback(() => {
    setHasLoaded(false);
  }, [refreshing]);

  if (constructor) {
    getUserVouchers()
      .then(response => {
        setVoucherList(response);
      })
      .finally(setConstructor(false));
  }

  if (!hasLoaded) {
    getUserId().then(userId => {
      callAPI(
        'get',
        'getVouchers',
        '?type=user&id=' + userId + '&status=active',
      )
        .then(response => {
          let _voucherList = [];

          for (let i = 0; i < response.Items.length; i++) {
            let voucher = response.Items[i];

            let _voucher = {
              key: i,
              promoId: voucher.promotion.id,
              name: voucher.promotion.promoName,
              company: voucher.companyProfile.companyName,
              img: { uri: voucher.promotion.resourceUrl },
              id: voucher.id,
              startDate: voucher.voucherStartDate,
              confirmed: voucher.hasBeenConfirmed === 'true',
              endDate: voucher.voucherEndDate,
              description: voucher.promotion.description,
              rules: voucher.promotion.prules,
            };

            _voucherList.push(_voucher);
          }

          console.log(_voucherList);
          setUserVouchers(_voucherList);
          setVoucherList(_voucherList);
          setHasLoaded(true);
        })
        .catch(error => {
          console.log('error', error);
        });
    });
  }

  const renderItem = (title, content, isOpen, setIsOpen) => {
    return (
      <TouchableOpacity style={styles.item} onPress={() => setIsOpen(!isOpen)}>
        <View style={{ flexDirection: 'row' }}>
          <Icon
            style={{ color: Colours.DARK_BLUE, marginLeft: 10 }}
            name={isOpen ? 'arrow-up' : 'arrow-down'}
          />
          <Text style={styles.headlineItem}> {title}</Text>
        </View>

        {isOpen ? <Text style={styles.textItem}>{content}</Text> : null}
        <Divider style={{ height: 2, marginTop: 15 }} />
      </TouchableOpacity>
    );
  };

  if (renderQRCode) {
    console.log(selectedOffer, voucherList);
    let {
      img,
      id,
      name,
      company,
      description,
      rules,
      startDate,
      endDate,
    } = voucherList[selectedOffer];

    return (
      <Container style={{ flex: 1 }}>
        <ScrollView>
          <FormHeader
            title={company}
            onPressBack={() => {
              setRenderQRCode(false);
            }}
          />
          <CardItem cardBody>
            <ProgressiveImage source={img} style={styles.voucherImg} />
          </CardItem>
          <Card transparent style={styles.card}>
            <View style={styles.cardDetails}>
              <Text style={styles.headline}>{name}</Text>
              <Text style={styles.text}>Voucher</Text>
              <View style={styles.container}>
                <QRCode value={id} size={200} />
              </View>
            </View>

            {renderItem(
              'Descricao',
              description,
              isOpenDescription,
              setIsOpenDescription,
            )}
            {renderItem(
              'Regras da Promocao',
              rules,
              isOpenRules,
              setIsOpenRules,
            )}
            {renderItem(
              'Validade do Voucher',
              formatDate(startDate) + ' a ' + formatDate(endDate),
              isOpenValid,
              setIsOpenValid,
            )}
          </Card>
        </ScrollView>
      </Container>
    );
  }

  return (
    <Container>
      <FormHeader title="Vouchers" onRefresh={onRefresh} />
      <Loader isLoading={!hasLoaded} />
      <ScrollView
        contentContainerStyle={{ paddingVertical: 20 }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {hasLoaded
          ? voucherList.map(
              ({ name, img, startDate, endDate, confirmed, key }) => (
                <Card transparent style={styles.card}>
                  <CardItem cardBody>
                    <ProgressiveImage
                      source={img}
                      style={styles.cardImage}
                      onPress={() => {
                        if (confirmed) {
                          selectedOffer = key;
                          setRenderQRCode(true);
                        }
                      }}
                    />
                  </CardItem>

                  <View style={styles.cardDetails}>
                    <Text style={styles.headline}>{name}</Text>
                    <Text style={styles.text}>
                      Validade {formatDate(startDate)} a {formatDate(endDate)}.
                    </Text>

                    {!confirmed ? (
                      <Badge style={styles.badge}>
                        <Text>Pendente Confirmacao</Text>
                      </Badge>
                    ) : (
                      <Badge style={styles.badgeConfirmed}>
                        <Text>Voucher Confirmado</Text>
                      </Badge>
                    )}
                  </View>
                  <Divider />
                </Card>
              ),
            )
          : null}
      </ScrollView>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  card: {
    marginRight: 10,
    marginLeft: 10,
  },
  headline: {
    fontWeight: 'bold',
    fontSize: 20,
    color: Colours.ORANGE,
    marginTop: 7,
  },
  text: {
    fontSize: 15,
    marginTop: 7,
    color: Colours.DARK_BLUE,
  },
  cardImage: {
    height: 150,
    width: null,
    flex: 1,
    borderRadius: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  voucherImg: {
    height: 200,
    width: null,
    flex: 1,
    marginTop: 5,
  },
  cardDetails: {
    marginLeft: 10,
    marginRight: 10,
  },
  badge: {
    backgroundColor: Colours.ORANGE,
    marginTop: 7,
    marginBottom: 7,
  },
  badgeConfirmed: {
    backgroundColor: Colours.DARK_BLUE,
    marginTop: 7,
    marginBottom: 7,
  },
  item: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 15,
  },
  headlineItem: {
    fontWeight: 'bold',
    color: Colours.DARK_BLUE,
    marginTop: 7,
  },
  textItem: {
    marginTop: 7,
    marginLeft: 10,
    marginBottom: 10,
    color: Colours.DARK_BLUE,
  },
});

export default Voucher;
