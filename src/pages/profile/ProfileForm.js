import React, { useState } from 'react';
import {
  Dimensions,
  StyleSheet,
  View,
  SafeAreaView,
  Platform,
} from 'react-native';

import { Form, Item, Picker, Input, Label } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Loader from '../../components/Loader';
import DateField from '../../components/DateField';
import { Colours } from '../../styles/Styles';

let isLoading = false;

const FormComponent = props => {
  console.log('props state', props.state.userName);

  const [userName, setUserName] = useState(props.state.userName);
  const [phoneNumber, setPhoneNumber] = useState(props.state.phoneNumber);
  const [address, setAddress] = useState(props.state.address);
  const [city, setCity] = useState(props.state.city);
  const [uState, setuState] = useState(props.state.uState);
  const [sex, setSex] = useState(props.state.sex);
  const [bornDate, setBornDate] = useState(props.state.bornDate);

  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <View
        style={styles.scrollView}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View>
          <Form>
            <Item underline={false}>
              <Label style={styles.label}>Nome</Label>
              <Input
                value={userName}
                style={styles.input}
                placeholderTextColor={Colours.PLACEHOLDER_LIGHT}
                onChangeText={text => {
                  setUserName(text);
                  props.setFieldValue({ userName: text });
                }}
              />
            </Item>
                      
            <Item>
              <Label style={styles.label}>Cidade</Label>
              <Input
                value={city}
                placeholder="Caxias do Sul"
                placeholderTextColor={Colours.PLACEHOLDER_LIGHT}
                style={styles.input}
                onChangeText={text => {
                  setCity(text);
                  props.setFieldValue({ city: text });
                }}
              />
            </Item>

            <Item picker style={{ paddingLeft: 15 }}>
              <Label style={styles.labelDropDown}>Estado</Label>
              <Picker
                mode="dropdown"
                iosIcon={
                  <Icon
                    style={{ color: Colours.PLACEHOLDER_LIGHT }}
                    name="arrow-down"
                  />
                }
                textStyle={{ color: Colours.DARK_BLUE }}
                placeholder="Selecione seu estado"
                placeholderStyle={{ color: Colours.PLACEHOLDER_LIGHT }}
                placeholderIconColor={Colours.PLACEHOLDER_LIGHT}
                selectedValue={uState}
                onValueChange={(itemValue, itemIndex) => {
                  setuState(itemValue);
                  props.setFieldValue({ uState: itemValue });
                }}>
                <Picker.Item label="Acre" value="AC" />
                <Picker.Item label="Alagoas" value="AL" />
                <Picker.Item label="Amapa" value="AP" />
                <Picker.Item label="Amazonas" value="AM" />
                <Picker.Item label="Bahia" value="BA" />
                <Picker.Item label="Ceará" value="CE" />
                <Picker.Item label="Distrito Federal" value="DF" />
                <Picker.Item label="Espírito Santo" value="ES" />
                <Picker.Item label="Goiás" value="GO" />
                <Picker.Item label="Maranhão" value="MA" />
                <Picker.Item label="Mato Grosso" value="MT" />
                <Picker.Item label="Mato Grosso do Sul" value="MS" />
                <Picker.Item label="Minas Gerais" value="MG" />
                <Picker.Item label="Pará" value="PA" />
                <Picker.Item label="Paraíba" value="PB" />
                <Picker.Item label="Paraná" value="PR" />
                <Picker.Item label="Pernambuco" value="PE" />
                <Picker.Item label="Piauí" value="PI" />
                <Picker.Item label="Rio de Janeiro" value="RJ" />
                <Picker.Item label="Rio Grande do Norte" value="RN" />
                <Picker.Item label="Rio Grande do Sul" value="RS" />
                <Picker.Item label="Rondônia" value="RO" />
                <Picker.Item label="Roraima" value="RR" />
                <Picker.Item label="Santa Catarina" value="SC" />
                <Picker.Item label="São Paulo" value="SP" />
                <Picker.Item label="Sergipe" value="SE" />
                <Picker.Item label="Tocantins" value="TO" />
              </Picker>
            </Item>

            <Item picker style={{ paddingLeft: 15 }}>
              <Label style={styles.labelDropDown}>Sexo</Label>
              <Picker
                mode="dropdown"
                iosIcon={
                  <Icon
                    name="arrow-down"
                    style={{ color: Colours.PLACEHOLDER_LIGHT }}
                  />
                }
                style={{ width: undefined }}
                textStyle={{ color: Colours.DARK_BLUE }}
                placeholder="Selecione seu sexo"
                placeholderStyle={{ color: Colours.PLACEHOLDER_LIGHT }}
                placeholderIconColor={Colours.PLACEHOLDER_LIGHT}
                selectedValue={sex}
                onValueChange={(itemValue, itemIndex) => {
                  setSex(itemValue);
                  props.setFieldValue({ sex: itemValue });
                }}>
                <Picker.Item label="Feminino" value="Feminino" />
                <Picker.Item label="Masculino" value="Masculino" />
              </Picker>
            </Item>

            <DateField
              bornDate={bornDate}
              label="Dt. Nasc."
              onChange={text => {
                console.log(text);
                setBornDate(text);
                props.setFieldValue({ bornDate: text });
              }}
            />
          </Form>
        </View>
        {props.children}
      </View>
    </SafeAreaView>
  );
};

export default FormComponent;

const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    // flex: 1,
    marginTop: 35,
    marginBottom: 30,
    width: width - 15,
    height: height - (Platform.OS === 'ios' ? 400 : 250),
  },
  text: {
    fontSize: 42,
  },
  label: {
    width: 90,
    color: Colours.ORANGE,
  },
  labelDropDown: {
    width: 80,
    color: Colours.ORANGE,
  },
  input: {
    color: Colours.DARK_BLUE,
  },
  item: {
    borderWidth: 0,
  },
  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
    backgroundColor: '#0275d8',
  },
});
