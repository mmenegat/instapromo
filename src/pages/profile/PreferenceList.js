import React, { useState } from 'react';
import { StyleSheet, View, Image, TouchableHighlight } from 'react-native';
import { Text } from 'native-base';

import cafeLogo from '../../assets/Cafe.png';
import foodLogo from '../../assets/Culinaria.png';
import sportsLogo from '../../assets/Esportes.png';
import gamesLogo from '../../assets/Games.png';
import musicLogo from '../../assets/Musica.png';
import othersLogo from '../../assets/Outros.png';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Colours } from '../../styles/Styles';
import ProgressiveImage from '../../components/ProgressiveImage';

const Preference = props => {
  const [isSelected, setIsSelected] = useState(props.isSelected);
  const { text, updateProps } = props;

  const onChange = () => {
    setIsSelected(!isSelected);
    updateProps(text);
  };

  let logo;

  switch (text) {
    case 'Musica':
      logo = musicLogo;
      break;
    case 'Cafe':
      logo = cafeLogo;
      break;
    case 'Esportes':
      logo = sportsLogo;
      break;
    case 'Culinaria':
      logo = foodLogo;
      break;
    case 'Games':
      logo = gamesLogo;
      break;
    case 'Outros':
      logo = othersLogo;
      break;
    default:
      break;
  }

  return (
    <TouchableHighlight onPress={onChange}>
      <>
        <Image
          source={logo}
          style={isSelected ? styles.selectedImage : styles.image}
        />
        <View style={styles.imageContainer}>
          <Text style={styles.imageText}>{text}</Text>
        </View>
      </>
    </TouchableHighlight>
  );
};

const PreferenceList = props => {
  const interests = props.state.interests;

  const [preferences, setPreferences] = useState(
    interests > '' ? interests.split(',') : [],
  );

  const renderOption = text => {
    const onChange = text => {
      let prefList = preferences;
      if (prefList.indexOf(text) > -1) {
        let filtered = prefList.filter((value, index, arr) => {
          return value != text;
        });
        props.setFieldValue({ interests: filtered.toString() });
        setPreferences(filtered);
      } else {
        prefList.push(text);
        props.setFieldValue({ interests: prefList.toString() });
        setPreferences(prefList);
      }
    };

    return (
      <Preference
        text={text}
        updateProps={onChange}
        isSelected={interests.indexOf(text) > -1}
      />
    );
  };

  return (
    <View style={styles.container}>
      <Grid style={{ height: 475 }}>
        <Col style={{ height: 475 }}>
          <Row style={styles.rowContentLeft}>{renderOption('Musica')}</Row>
          <Row style={styles.rowContentLeft}>{renderOption('Esportes')}</Row>
          <Row style={styles.rowContentLeft}>{renderOption('Games')}</Row>
        </Col>
        <Col style={{ height: 475 }}>
          <Row style={styles.rowContentRight}>{renderOption('Cafe')}</Row>
          <Row style={styles.rowContentRight}>{renderOption('Culinaria')}</Row>
          <Row style={styles.rowContentRight}>{renderOption('Outros')}</Row>
        </Col>
      </Grid>

      <View style={{ marginTop: 475 }}>{props.children}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  option: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
  },
  container: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
  },
  rowContentLeft: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginRight: 7,
  },
  rowContentRight: {
    alignItems: 'center',
    marginLeft: 7,
  },
  selectedImage: {
    borderColor: Colours.ORANGE,
    borderWidth: 4,
    borderRadius: 10,
  },
  image: {},
  imageContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageText: {
    fontSize: 30,
    color: Colours.WHITE,
  },
});

export default PreferenceList;
