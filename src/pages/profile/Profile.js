import React, { Component } from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import { getUsername, getUserId, onSignOut } from '../auth/auth';
import { callAPI } from '../auth/AwsAPI';
import { Auth } from 'aws-amplify';
import ProfileForm from './ProfileForm';
import Loader from '../../components/Loader';
import PreferenceList from './PreferenceList';
import FormHeader from '../../components/FormHeader';
import Avatar from '../../components/Avatar';
import UserName from '../../components/UserName';
import { Text, Button, Toast } from 'native-base';
import { Colours } from '../../styles/Styles';
import { isIphoneX } from '../../util/util';

let isIOS = Platform.OS === 'ios';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 0,
      instaUsername: '',
      isAuthenticatedWithAWS: 'false',
      errorAuth: '',
      isLoading: false,
      apiMsg: '',
      userName: '',
      phoneNumber: '',
      address: '',
      interests: '',
      city: 'Caxias do Sul',
      uState: 'RS',
      sex: 'Masculino',
      bornDate: '',
      id: '',
    };
  }

  componentDidMount() {
    getUsername()
      .then(res => {
        this.setState({ instaUsername: res });
      })
      .catch(err => alert('An error occurred'));

    getUserId()
      .then(res => {
        console.log(res);
        this.setState({ id: res });
        this.getUserData();
      })
      .catch(err => alert('An error occurred'));
  }

  _signOut = async () => {
    await Auth.signOut();
    await onSignOut();
    this.props.navigation.navigate('SignedOut');
  };

  getUserData = () => {
    this.setState({ isLoading: true });
    callAPI('post', 'getuserprofile', { body: { id: this.state.id } })
      .then(response => {
        if (response.Item) {
          let {
            address,
            bornDate,
            city,
            userName,
            phoneNumber,
            sex,
            uState,
          } = response.Item;
          let interests = response.Item.interests
            ? response.Item.interests
            : '';
          this.setState({
            address,
            bornDate,
            city,
            userName,
            phoneNumber,
            sex,
            uState,
            interests,
          });
        }

        this.setState({ isLoading: false });
        console.log('resp Here', response);
      })
      .catch(error => {
        this.setState({ isLoading: false });
        console.log('err', error);
      });
  };

  continueToNextStep = () => {
    let {
      userName,
      city,
      uState,
      sex,
      bornDate,
    } = this.state;

    if (
      !(
        userName > '' &&
        city > '' &&
        uState > '' &&
        sex > '' &&
        bornDate > ''
      )
    ) {
      Toast.show({
        text:
          'Todos os campos devem ser preenchidos para prosseguir!',
        buttonText: 'ok',
        type: 'danger',
        duration: 5000,
      });
      return;
    }

    this.setState({ step: 1 });
  };

  saveProfile = () => {
    let {
      id,
      userName,
      city,
      uState,
      sex,
      bornDate,
      instaUsername,
      interests,
    } = this.state;
    let body = {
      id,
      userName,
      city,
      uState,
      sex,
      bornDate,
      instaUsername,
      interests,
    };

    callAPI('post', 'userprofile', { body: body })
      .then(response => {
        this.setState({ isLoading: false });
        if (this.props.navigation.state.routeName === 'SignUp') {
          this.props.navigation.navigate('SignedIn');
        }
      })
      .catch(error => {
        console.log('error', error.response);
        this.setState({ isLoading: false });
      });
  };

  setFieldValue = value => {
    console.log('value', value);
    this.setState(value);
  };

  render() {
    let { step } = this.state;
    let type = isIOS ? 'ios' : step === 1 ? 'android' : 'ios';

    return (
      <View style={styles.container}>
        <Loader isLoading={this.state.isLoading} mode="modal" />

        {step === 0 ? (
          <FormHeader title="Perfil" />
        ) : (
          <FormHeader
            title="Preferências"
            onPressBack={() => {
              this.setState({ step: 0 });
            }}
          />
        )}

        {this.state.step === 0 && <Avatar type="login" />}

        <View>
          {this.state.step === 0 ? (
            this.state.isLoading ? null : (
              <View style={styles.bodyContent}>
                <UserName type={type} />
                <ProfileForm
                  setFieldValue={this.setFieldValue}
                  state={this.state}>
                  <Button
                    block
                    style={styles.actionButton}
                    onPress={this.continueToNextStep}>
                    <Text>Continuar</Text>
                  </Button>
                </ProfileForm>
              </View>
            )
          ) : (
            <PreferenceList
              setFieldValue={this.setFieldValue}
              state={this.state}>
              <Button
                block
                style={styles.actionButton}
                onPress={this.saveProfile}>
                <Text>Salvar Preferencias</Text>
              </Button>
            </PreferenceList>
          )}
        </View>
      </View>
    );
  }
}

const iphoneX = isIphoneX();
const styles = StyleSheet.flatten({
  container: {
    flex: 1,
    backgroundColor: Colours.WHITE,
  },
  bodyContent: {
    flex: 1,
    paddingTop: iphoneX ? 110 : 60,
  },
  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
  },
  actionButton: {
    marginLeft: 25,
    marginRight: 30,
    borderRadius: 10,
    backgroundColor: Colours.ORANGE,
    marginBottom: 20,
    marginTop: 20,
  },
});
