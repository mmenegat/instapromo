import React, { Component } from 'react';
import { StyleSheet, View, Alert, Dimensions } from 'react-native';
import { setUserName, setProfilePhoto } from '../auth/auth';
import axios from 'axios';
import { Text, Button, Form, Item, Input, Toast } from 'native-base';
import { Colours } from '../../styles/Styles';

export default class UpdateUserName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
    };
  }

  continueToNextStep = async () => {
    let userName = this.state.userName;

    if(userName.charAt(0) === "@"){
      userName = userName.substring(1);
    }

    if (userName > '') {
      setUserName(userName);

      try{
        const result = await axios.get('https://www.instagram.com/' + userName + '/?__a=1');
        setProfilePhoto(result.data.graphql.user.profile_pic_url);
        this.props.navigation.navigate('Profile');
        return;

      }catch(e){

        console.log("errorr", e, userName);
        Alert.alert(
          'Erro',
          'Usuario nao encontrado. Verifique o valor informado e tente novamente!',
          [{ text: 'Ok', style: 'cancel' }],
          { cancelable: false },
        );
        this.props.navigation.navigate('Profile');
       
      }

   /*   axios
        .get('https://www.instagram.com/' + userName + '/?__a=1')
        .then(result => {
          console.log("result", result)
          setProfilePhoto(result.data.graphql.user.profile_pic_url);
          this.props.navigation.navigate('Profile');
          return;
        })
        .catch(err => {
          console.log("err", err, userName);
          Alert.alert(
            'Erro',
            'Usuario nao encontrado. Verifique o valor informado e tente novamente!',
            [{ text: 'Ok', style: 'cancel' }],
            { cancelable: false },
          );
        });*/
    } else {
      Toast.show({
        text:
          'Por favor, informe o nome de usuario do Instragram para prosseguir!',
        buttonText: 'ok',
        type: 'danger',
        duration: 3000,
      });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.headText}>Login realizado com Sucesso!</Text>

        <Text style={styles.text}>
          Agora precisamos que nos informe o seu nome de usuario no Instagram.
        </Text>
        <Text style={styles.text}>
          Esta informação é importante para que possamos validar suas postagens
          e garantir as emissões de vouchers!
        </Text>

        <View style={styles.userName}>
          <Form style={{ marginRight: 40 }}>
            <Item>
              <Input
                placeholder="heardbout"
                style={styles.formItem}
                placeholderTextColor={Colours.PLACEHOLDER_LIGHT}
                value={this.state.userName}
                onChangeText={text => this.setState({ userName: text })}
              />
            </Item>
          </Form>
        </View>

        <Button
          block
          style={styles.actionButton}
          onPress={this.continueToNextStep}>
          <Text>Continuar</Text>
        </Button>
      </View>
    );
  }
}

const { width } = Dimensions.get('window');
const styles = StyleSheet.flatten({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    margin: 20,
  },
  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
    backgroundColor: '#0275d8',
  },
  headText: {
    marginBottom: 40,
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    color: Colours.ORANGE,
  },
  text: {
    marginLeft: 20,
    marginRight: 20,
    textAlign: 'justify',
    marginBottom: 5,
    color: Colours.DARK_BLUE,
  },
  userName: {
    marginTop: 20,
    width: width - 40,
    marginBottom: 40,
  },
  actionButton: {
    marginLeft: 25,
    marginRight: 30,
    borderRadius: 10,
    backgroundColor: Colours.ORANGE,
    marginBottom: 20,
  },
  formItem: {
    color: Colours.DARK_BLUE,
  },
});
