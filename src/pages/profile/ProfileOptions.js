import React, { Component } from 'react';
import { StyleSheet, View, Alert, BackHandler, Dimensions } from 'react-native';
import { getUsername, getUserId, onSignOut } from '../auth/auth';
import { callAPI } from '../auth/AwsAPI';
import { Auth } from 'aws-amplify';
import ProfileForm from './ProfileForm';
import Loader from '../../components/Loader';
import PreferenceList from './PreferenceList';
import FormHeader from '../../components/FormHeader';
import Avatar from '../../components/Avatar';
import UserName from '../../components/UserName';
import { Container, Text, Button } from 'native-base';

import { Colours } from '../../styles/Styles';
import { isIphoneX } from '../../util/util';
import { ConfirmMessage, ShowMessage } from '../../components/Modal';

export default class ProfileOptions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 0,
      view: 'main',
      instaUsername: '',
      isAuthenticatedWithAWS: 'false',
      errorAuth: '',
      isLoading: false,
      apiMsg: '',
      userName: '',
      phoneNumber: '',
      address: '',
      city: '',
      uState: 'AM',
      sex: 'Masculino',
      bornDate: '',
      interests: '',
      id: '',
      confirmMessage: {
        isOpen: false,
      },
      message: {
        isOpen: false,
      },
    };
  }

  renderBackButton = () => {
    this.setState({ view: 'main' });
    return true;
  };

  componentWillUpdate() {
    if (
      this.state.view === 'Preferences' ||
      this.state.view === 'UserProfile'
    ) {
      BackHandler.addEventListener('hardwareBackPress', this.renderBackButton);
    }

    if (this.state.view === 'main') {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        this.renderBackButton,
      );
    }
  }

  componentDidMount() {
    getUsername()
      .then(res => {
        console.log(res);
        this.setState({ instaUsername: res });
      })
      .catch(err => alert('An error occurred'));

    getUserId()
      .then(res => {
        console.log(res);
        this.setState({ id: res });
        //this.getUserData()
      })
      .catch(err => alert('An error occurred'));
  }

  _signOut = async () => {
    await Auth.signOut();
    await onSignOut();
    this.props.navigation.navigate('SignedOut');
  };

  signOut = () => {
    this.setState({
      confirmMessage: {
        isOpen: true,
        title: 'Logout',
        description: 'Você será redirecionado para a página de login.',
        onConfirm: () => {
          this.closeMessage();
          this._signOut();
        },
        onCancel: this.closeMessage,
      },
    });
  };

  getUserData = () => {
    this.setState({ isLoading: true });
    callAPI('post', 'getuserprofile', { body: { id: this.state.id } })
      .then(response => {
        if (response.Item) {
          let {
            address,
            bornDate,
            city,
            userName,
            phoneNumber,
            sex,
            uState,
          } = response.Item;
          let interests = response.Item.interests
            ? response.Item.interests
            : '';
          this.setState({
            address,
            bornDate,
            city,
            userName,
            phoneNumber,
            sex,
            uState,
            interests,
          });
        }
        this.setState({ isLoading: false });
        console.log('resp', response);
      })
      .catch(error => {
        this.setState({ isLoading: false });
        console.log('err', error);
      });
  };

  updateProfileData = () => {
    let {
      userName,
      phoneNumber,
      address,
      city,
      uState,
      sex,
      bornDate,
    } = this.state;

    if (
      !(
        userName > '' &&
        city > '' &&
        uState > '' &&
        sex > '' &&
        bornDate > ''
      )
    ) {
      Alert.alert(
        'Erro',
        'Todos os campos devem ser preenchidos para prosseguir!',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        { cancelable: false },
      );
      return;
    }

    this.saveProfile();
  };

  saveProfile = () => {
    let {
      id,
      userName,
      city,
      uState,
      sex,
      bornDate,
      instaUsername,
      interests,
    } = this.state;
    let body = {
      id,
      userName,
      city,
      uState,
      sex,
      bornDate,
      instaUsername,
      interests,
    };

    this.setState({ isLoading: true });

    callAPI('post', 'userprofile', { body: body })
      .then(response => {
        this.setState({ isLoading: false, view: 'main' });
        if (this.props.navigation.state.routeName === 'SignUp') {
          this.props.navigation.navigate('SignedIn');
        }
      })
      .catch(error => {
        console.log('error', error);
        this.setState({ isLoading: false, view: 'main' });
      });
  };

  setFieldValue = value => {
    this.setState(value);
  };

  updateUserProfile = () => {
    this.getUserData();
    this.setState({ view: 'UserProfile' });
  };

  updatePreferences = () => {
    this.getUserData();
    this.setState({ view: 'Preferences' });
  };

  closeMessage = () => {
    this.setState({
      confirmMessage: {
        isOpen: false,
      },
      message: {
        isOpen: false,
      },
    });
  };

  render() {
    let { view } = this.state;
    // let type = isIOS ? "ios" : step === 1 ? "android" : ""
    let type = 'ios';

    if (this.state.view === 'UserProfile') {
      return (
        <Container>
          <Loader isLoading={this.state.isLoading} mode="modal" />
          <FormHeader
            title="Perfil"
            onPressBack={() => {
              this.setState({ view: 'main' });
            }}
          />
          <Avatar type={type} />

          <View>
            <View style={styles.bodyContent}>
              {!this.state.isLoading && (
                <ProfileForm
                  setFieldValue={this.setFieldValue}
                  state={this.state}>
                  <Button
                    block
                    style={styles.actionButton}
                    onPress={this.updateProfileData}>
                    <Text>Salvar</Text>
                  </Button>
                </ProfileForm>
              )}
            </View>
          </View>
        </Container>
      );
    }

    if (this.state.view === 'Preferences') {
      return (
        <Container>
          <Loader isLoading={this.state.isLoading} mode="modal" />
          <FormHeader
            title="Preferências"
            onPressBack={() => {
              this.setState({ view: 'main' });
            }}
          />
          <View>
            <View>
              {!this.state.isLoading && (
                <PreferenceList
                  setFieldValue={this.setFieldValue}
                  state={this.state}>
                  <Button
                    block
                    style={styles.actionButton}
                    onPress={this.updateProfileData}>
                    <Text>Salvar</Text>
                  </Button>
                </PreferenceList>
              )}
            </View>
          </View>
        </Container>
      );
    }

    return (
      <View style={styles.container}>
        <Loader isLoading={this.state.isLoading} mode="modal" />
        <ConfirmMessage {...this.state.confirmMessage} />

        <FormHeader title="Perfil" onLogout={this.signOut} />
        <Avatar />
        <View style={styles.body}>
          <View style={styles.bodyContentMain}>
            <UserName type="large" />

            <Button
              block
              style={styles.actionButton}
              onPress={this.updateUserProfile}>
              <Text>Editar perfil</Text>
            </Button>

            <Button
              block
              style={styles.actionButton}
              onPress={this.updatePreferences}>
              <Text>Editar preferências</Text>
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

const { height } = Dimensions.get('window');
const iphoneX = isIphoneX();
const styles = StyleSheet.create({
  container: {
    marginBottom: 30,
    height: height,
  },
  body: {
    marginTop: iphoneX ? 140 : 120,
  },
  bodyContent: {
    paddingTop: iphoneX ? 150 : 80,
  },
  bodyContentMain: {
    padding: iphoneX ? 30 : 0,
  },
  actionButton: {
    marginLeft: 25,
    marginRight: 30,
    borderRadius: 10,
    backgroundColor: Colours.ORANGE,
    marginTop: 15,
  },
});
