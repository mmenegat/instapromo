import React, { useState } from 'react';
import {
  getUserId,
  getUserHistory,
  setUserHistory,
  getUserVouchers,
  setUserVouchers,
} from '../auth/auth';
import { callAPI } from '../auth/AwsAPI';
import Loader from '../../components/Loader';
import {
  ScrollView,
  View,
  RefreshControl,
  StyleSheet,
  Image,
} from 'react-native';
import { formatDate } from '../../util/util';
import { Container, Card, CardItem, Text, Badge } from 'native-base';
import FormHeader from '../../components/FormHeader';
import Divider from '../../components/Divider';
import { Colours } from '../../styles/Styles';
import ProgressiveImage from '../../components/ProgressiveImage';

const History = () => {
  const [voucherList, setVoucherList] = useState([]);
  const [hasLoaded, setHasLoaded] = useState(false);
  const [constructor, setConstructor] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = React.useCallback(() => {
    setHasLoaded(false);
  }, [refreshing]);

  if (constructor) {
    getUserHistory()
      .then(response => {
        setVoucherList(response);
      })
      .finally(setConstructor(false));
  }

  if (!hasLoaded) {
    getUserId().then(userId => {
      getUserVouchers().then(userVouchers => {
        callAPI('get', 'getVouchers', '?type=user&id=' + userId + '&status=all')
          .then(response => {
            let _voucherList = [];
            let _historyVoucherList = [];

            for (let i = 0; i < response.Items.length; i++) {
              let voucher = response.Items[i];

              let _voucher = {
                key: i,
                name: voucher.promotion.promoName,
                promoId: voucher.promotion.id,
                img: { uri: voucher.promotion.resourceUrl },
                id: voucher.id,
                startDate: voucher.voucherStartDate,
                endDate: voucher.voucherEndDate,
                validated: voucher.hasBeenValidated === 'true',
                reason: voucher.reason,
                rejected: voucher.hasBeenRejected === 'true',
                allowNewPost: voucher.allowNewPost === 'true',
              };

              _voucherList.push(_voucher);
              if (_voucher.allowNewPost) {
                userVouchers = userVouchers.filter(item => {
                  return item.id !== voucher.id;
                });
              } else {
                _historyVoucherList.push(_voucher);
              }
            }

            setUserHistory(_historyVoucherList);
            setVoucherList(_voucherList);
            setUserVouchers(userVouchers);
            setHasLoaded(true);
          })
          .catch(error => {
            console.log('error', error);
          });
      });
    });
  }

  return (
    <Container style={{ flex: 1 }}>
      <FormHeader title="Histórico" onRefresh={onRefresh} />
      <Loader isLoading={!hasLoaded} />
      <ScrollView
        contentContainerStyle={{ paddingVertical: 20 }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {hasLoaded
          ? voucherList.map(
              ({
                name,
                img,
                startDate,
                endDate,
                validated,
                rejected,
                reason,
                allowNewPost,
                key,
              }) => (
                <Card transparent style={styles.card}>
                  <CardItem cardBody>
                    <ProgressiveImage source={img} style={styles.cardImage} />
                  </CardItem>

                  <View style={styles.cardDetails}>
                    <Text style={styles.headline}>{name}</Text>
                    {validated ? (
                      <Badge style={styles.badgeConfirmed}>
                        <Text>Voucher utilizado</Text>
                      </Badge>
                    ) : rejected ? (
                      <>
                        <Badge style={styles.badgeError}>
                          <Text>Voucher Rejeitado</Text>
                        </Badge>
                        <Text style={styles.text}>
                          <Text style={styles.textBold}>Motivo:</Text> {reason}
                        </Text>
                        <Text style={styles.text}>
                          <Text style={styles.textBold}>
                            Permite Repostagem:
                          </Text>{' '}
                          {allowNewPost ? 'Sim' : 'Nao'}
                        </Text>
                      </>
                    ) : (
                      <>
                        <Badge style={styles.badge}>
                          <Text>Voucher Expirado</Text>
                        </Badge>
                        <Text style={styles.text}>
                          <Text style={styles.textBold}>Validade:</Text>{' '}
                          {formatDate(startDate)} a {formatDate(endDate)}.
                        </Text>
                      </>
                    )}
                  </View>
                  <Divider />
                </Card>
              ),
            )
          : null}
      </ScrollView>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  card: {
    marginRight: 10,
    marginLeft: 10,
  },
  headline: {
    fontWeight: 'bold',
    fontSize: 20,
    color: Colours.ORANGE,
    marginTop: 7,
  },
  textBold: {
    fontSize: 15,
    color: Colours.DARK_BLUE,
    fontWeight: 'bold',
  },
  text: {
    fontSize: 15,
    marginTop: 7,
    marginBottom: 7,
    color: Colours.DARK_BLUE,
  },
  cardImage: {
    height: 150,
    width: null,
    flex: 1,
    borderRadius: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  voucherImg: {
    height: 200,
    width: null,
    flex: 1,
    marginTop: 5,
  },
  cardDetails: {
    marginLeft: 10,
    marginRight: 10,
  },
  badge: {
    backgroundColor: Colours.ORANGE,
    marginTop: 7,
    marginBottom: 7,
  },
  badgeConfirmed: {
    backgroundColor: Colours.DARK_BLUE,
    marginTop: 7,
    marginBottom: 7,
  },
  badgeError: {
    backgroundColor: Colours.RED,
    marginTop: 7,
    marginBottom: 7,
  },
});

export default History;
