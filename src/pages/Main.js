import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { callAPI } from './auth/AwsAPI';

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apiMsg: '',
      userId: '',
    };
  }

  callTestAPI = () => {
    callAPI('get', 'test', '')
      .then(response => {
        this.setState({ apiMsg: response.toString() });
      })
      .catch(error => {
        this.setState({ apiMsg: error.toString() });
      });
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text> Home </Text>

        <TouchableOpacity
          style={{
            borderRadius: 5,
            backgroundColor: 'orange',
            height: 30,
            width: 100,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 10,
          }}
          onPress={this.callTestAPI}>
          <Text style={{ color: 'white', textAlign: 'center' }}>
            Chama API test
          </Text>
        </TouchableOpacity>

        <Text style={{ color: 'red', textAlign: 'center' }}>
          {this.state.apiMsg}
        </Text>
      </View>
    );
  }
}
