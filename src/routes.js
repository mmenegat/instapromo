import { createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Main from './pages/main';
import Login from './pages/auth/login';
import AuthLoadingScreen from './pages/auth/AuthLoadingScreen';

const AppStack = createStackNavigator({ Home: Main }); //, Other: OtherScreen
const AuthStack = createStackNavigator({ SignIn: Login });

export default createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);
